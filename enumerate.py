import math

digits = 6

def sumdigits(n):
    sum = 0
    n = int(n)
    while n > 0:
        sum += n % 10
        n = int(n / 10)
    return sum

assert(digits % 2 == 0)
winners = 0
start = int(math.pow(10, digits - 1))
end = int(math.pow(10, digits))
halflimit = int(math.pow(10, digits / 2))
for i in range(start, end):
    if sumdigits(i % halflimit) == sumdigits(i / halflimit):
        winners += 1

print("Probability = {}".format(winners / (end - start)))
